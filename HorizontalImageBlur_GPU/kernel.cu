
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <FreeImage.h>
#include <iostream>
#include <chrono>
#include <stdio.h>

// BatchLoad.cpp - Forward declaration
FIBITMAP* GenericLoader(const char* lpszPathName, int flag);
bool GenericWriter(FIBITMAP* dib, const char* lpszPathName, int flag);

__global__ void blurEdge(BYTE* source, int width, int height, int numImageBytes)
{
	int row = blockIdx.x;		// Row of the image
	int channel = threadIdx.x;  // Color channel of the image
	int blurAmount = 50;	    // Desired amount of blur
	float blur = 0;				// Calculated blur for this channel

								// Precalculations
	const int row3width = row * 3 * width;

	// Iterate over row
	for (int column = width - 1, i = 1; column >= width - blurAmount; --column, i++)
	{
		// Precalculations
		const int idx = row3width + column * 3 + channel;

		// Copy this pixel
		int color = source[idx];

		// Add this pixel to blur
		blur += color;

		// Update this pixel
		source[idx] = (int)(color / 2.0f + blur * (0.5f / i));
	}
}

__global__ void blurNormal(BYTE* source, int width, int height, int numImageBytes)
{
	int row = blockIdx.x;
	int channel = threadIdx.x;
	int blurAmount = 50;

	// Precalculations
	const int row3width = row * 3 * width;

	// Calculate base blur
	float blur = 0;
	for (int b = 0; b < blurAmount; ++b)
	{
		blur += source[row3width + b * 3 + channel];
	}

	// Iterate over row
	for (int column = 0; column < width - blurAmount; ++column)
	{
		// Precalculations
		const int idx = row3width + column * 3 + channel;

		// Copy this pixel
		int color = source[idx];

		// Substract this pixel from blur
		blur -= color;

		// Add missing pixel to blur
		blur += source[row3width + (column + blurAmount - 1) * 3 + channel];

		// Update this pixel
		source[idx] = (int)(color / 2.0f + blur * (0.5f / blurAmount));
	}
}

__global__ void blurEdgeSlow(BYTE* source, int width, int height, int numImageBytes)
{
	for (int channel = 0; channel < 3; ++channel)
	{
		int row = blockIdx.x;		// Row of the image
		int blurAmount = 50;	    // Desired amount of blur
		float blur = 0;				// Calculated blur for this channel

		// Precalculations
		const int row3width = row * 3 * width;

		// Iterate over row
		for (int column = width - 1, i = 1; column >= width - blurAmount; --column, i++)
		{
			// Precalculations
			const int idx = row3width + column * 3 + channel;

			// Copy this pixel
			int color = source[idx];

			// Add this pixel to blur
			blur += color;

			// Update this pixel
			source[idx] = (int)(color / 2.0f + blur * (0.5f / i));
		}
	}
}

__global__ void blurNormalSlow(BYTE* source, int width, int height, int numImageBytes)
{
	for (int channel = 0; channel < 3; ++channel)
	{
		int row = blockIdx.x;
		int blurAmount = 50;

		// Precalculations
		const int row3width = row * 3 * width;

		// Calculate base blur
		float blur = 0;
		for (int b = 0; b < blurAmount; ++b)
		{
			blur += source[row3width + b * 3 + channel];
		}

		// Iterate over row
		for (int column = 0; column < width - blurAmount; ++column)
		{
			// Precalculations
			const int idx = row3width + column * 3 + channel;

			// Copy this pixel
			int color = source[idx];

			// Substract this pixel from blur
			blur -= color;

			// Add missing pixel to blur
			blur += source[row3width + (column + blurAmount - 1) * 3 + channel];

			// Update this pixel
			source[idx] = (int)(color / 2.0f + blur * (0.5f / blurAmount));
		}
	}
}


void applyBlur(const std::string input, const std::string output, const int options)
{
	std::cout << "Processing using CUDA" << std::endl;
	FIBITMAP* img = GenericLoader(input.c_str(), 0);

	const int height = FreeImage_GetHeight(img);
	const int width = FreeImage_GetWidth(img);
	const int size = height * width * sizeof(BYTE) * 3;

	BYTE* h_imgData = FreeImage_GetBits(img);

	// (0) create streams
	cudaStream_t streamNormal, streamEdge;
	cudaStreamCreate(&streamNormal); 
	cudaStreamCreate(&streamEdge);
	
	// (1) allocate device memory
	BYTE* d_imgData = 0;
	cudaMalloc((void**)&d_imgData, size);

	// (2) transfer memory to GPU
	cudaMemcpy(d_imgData, h_imgData, size, cudaMemcpyHostToDevice);

	auto start = std::chrono::steady_clock::now();

	// (3) run on GPU
	if (options == 1)
	{
		blurNormalSlow<<<height, 1, 0, streamNormal>>>(d_imgData, width, height, size);
		blurEdgeSlow<<<height, 1, 0, streamEdge>>>(d_imgData, width, height, size);
	}
	else
	{
		blurNormal<<<height, 3, 0, streamNormal>>>(d_imgData, width, height, size);
		blurEdge<<<height, 3, 0, streamEdge>>>(d_imgData, width, height, size);
	}

	cudaDeviceSynchronize();

	auto end = std::chrono::steady_clock::now();

	// (4) transfer memory from GPU
	cudaMemcpy(h_imgData, d_imgData, size, cudaMemcpyDeviceToHost);

	// (5) cleanup
	cudaFree(d_imgData);

	std::cout << "Elapsed time (w/o image read and write): "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
		<< " ms" << std::endl << std::endl;

	GenericWriter(img, output.c_str(), PNG_DEFAULT);
	FreeImage_Unload(img);
}

int main(int argc, char *argv[])
{
	if (argc != 4)
	{
		std::cout << "usage: HorizontalImageBlur_GPU.exe inputPath outputPath options" << std::endl;
	}
	else
	{
		std::string localPath = std::string(argv[0]);
		std::string input = std::string(argv[1]);
		std::string output = std::string(argv[2]);
		int options = std::atoi(argv[3]);

		// Remvoe *.exe from localPath
		localPath = localPath.substr(0, localPath.find_last_of("\\") + 1);

		applyBlur((localPath + input), (localPath + output), options);
	}
}
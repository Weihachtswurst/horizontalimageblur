#include "pch.h"

#include <FreeImage.h>
#include <omp.h>

#include <iostream>
#include <chrono>
#include <cstdlib>

// BatchLoad.cpp - Forward declaration
FIBITMAP* GenericLoader(const char* lpszPathName, int flag);
bool GenericWriter(FIBITMAP* dib, const char* lpszPathName, int flag);

void applyBlurOnRow(BYTE* img, int blurAmount, const int row, const int width)
{
	// Precalculations
	const int row3width = row * 3 * width;

	// Calculate base blur
	float blurRed = 0, blurGreen = 0, blurBlue = 0;
	for (int b = 0; b < blurAmount; ++b)
	{
		blurBlue  += img[row3width + b * 3 + 0];
		blurRed   += img[row3width + b * 3 + 1];
		blurGreen += img[row3width + b * 3 + 2];
	}

	// Iterate over row
	for (int column = 0; column < width; ++column)
	{
		// Precalculations
		const int column3 = column * 3;

		// Copy this pixel
		int blue = img[row3width + column3 + 0];
		int red = img[row3width + column3 + 1];
		int green = img[row3width + column3 + 2];

		// Substract this pixel from blur
		blurBlue -= blue;
		blurRed -= red;
		blurGreen -= green;

		if ((column + blurAmount - 1) < width)
		{
			// Add missing pixel to blur
			blurBlue += img[row3width + (column + blurAmount - 1) * 3 + 0];
			blurRed += img[row3width + (column + blurAmount - 1) * 3 + 1];
			blurGreen += img[row3width + (column + blurAmount - 1) * 3 + 2];
		}
		else
		{
			// Once the else branch gets executed, it will always get executed because column
			// keeps increasing at the same rate. The decreased blurAmount will make up for
			// the missing summands in the blur var's
			blurAmount--;
		}

		// Update this pixel
		img[row3width + column3 + 0] = (int)(blue / 2.0f + blurBlue * (0.5f / blurAmount));
		img[row3width + column3 + 1] = (int)(red / 2.0f + blurRed * (0.5f / blurAmount));
		img[row3width + column3 + 2] = (int)(green / 2.0f + blurGreen * (0.5f / blurAmount));
	}
}

#ifdef PC_THREADED
void applyBlur(FIBITMAP* img, const int blurAmount, const int numThreads)
#else
void applyBlur(FIBITMAP* img, const int blurAmount)
#endif
{
	// Get meta of image
	const auto width = FreeImage_GetWidth(img);
	const auto height = FreeImage_GetHeight(img);

	// Get the underlying data. I guess that's the fastest way?
	auto data = FreeImage_GetBits(img);

#ifdef PC_THREADED
	//std::cout << "compiled with openmp" << std::endl;
	#pragma omp parallel for num_threads(numThreads)
#endif
	for (int row = 0; row < height; row++)
	{
		// Apply blur row-wise
		applyBlurOnRow(data, blurAmount, row, width);
	}
}

void applyBlur(char *argv[])
{
	// Parse arguments
	std::string localPath = std::string(argv[0]);
	std::string input = std::string(argv[1]);
	std::string output = std::string(argv[2]);
	int numThreads = std::atoi(argv[3]);

	// Remvoe *.exe from localPath
	localPath = localPath.substr(0, localPath.find_last_of("\\") + 1);

	// Load image
	FIBITMAP* img = GenericLoader((localPath + input).c_str(), 0);

	auto start = std::chrono::steady_clock::now();

	// Check if image was successfully loaded
	if (img == NULL)
	{
		std::cout << "Failed to load image" << std::endl;
		return;
	}

#ifdef PC_THREADED
	// Apply blur with given numer of threads
	applyBlur(img, 50, numThreads);
#else
	// Apply blur single threaded
	applyBlur(img, 50);
#endif

	auto end = std::chrono::steady_clock::now();

	std::cout << "Elapsed time (w/o image read and write): "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
		<< " ms" << std::endl << std::endl;

	// Write the result back to disk
	GenericWriter(img, (localPath + output).c_str(), 0);
}
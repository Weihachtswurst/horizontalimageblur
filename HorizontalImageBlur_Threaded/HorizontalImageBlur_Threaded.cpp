#include "pch.h"
#include "../HorizontalImageBlur/BlurCPU.h"
#include <iostream>

int main(int argc, char *argv[])
{
	if (argc != 4)
	{
		std::cout << "usage: HorizontalImageBlur_Threaded.exe inputPath outputPath numOfThreads" << std::endl;
	}
	else
	{
		std::cout << "Processing using OpenMP" << std::endl;

		applyBlur(argv);
	}
}
// HorizontalImageBlur_Sequential.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"
#include "../HorizontalImageBlur/BlurCPU.h"
#include <iostream>

int main(int argc, char *argv[])
{
	if (argc != 4)
	{
		std::cout << "usage: HorizontalImageBlur_Sequential.exe inputPath outputPath numOfThreads" << std::endl;
	}
	else
	{
		std::cout << "Processing using single thread" << std::endl;

		applyBlur(argv);
	}
}
